## USBTIN can adapter

* [usbtin socketcan](https://www.fischl.de/usbtin/linux_can_socketcan/)


This repository contains easy installation scripts for usbtin.

run `sudo ./install.sh` to

* add udev rules
* install `usbtin.service` to automatically start device when it's plugged in.
