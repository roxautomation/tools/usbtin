#!/bin/bash

# Wait for the symlink to be established
while [ ! -e /dev/usbtin ]; do
    sleep 1
done

# Configure USBtin CAN interface
modprobe can
modprobe can-raw
modprobe slcan

# Use the symlink for slcan_attach, speed 500000 bps
slcan_attach -f -s6 -o /dev/usbtin
slcand -o -c -f /dev/usbtin slcan0
ifconfig slcan0 up
