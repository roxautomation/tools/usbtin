#!/bin/bash
set -e

# Ensure the running user is root
if [ "$(id -u)" -ne 0 ]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi


# Define the destination for the script and service file
START_USBTIN_DEST="/usr/local/bin/start_usbtin.sh"
SYSTEMD_SERVICE_FILE="/etc/systemd/system/usbtin.service"

# Function to check if can-utils is installed
check_canutils_installed() {
    if dpkg -l | grep -q "can-utils"; then
        echo "can-utils is already installed."
        return 0
    else
        echo "can-utils is not installed. Installing..."
        return 1
    fi
}

# Check and install can-utils if necessary
if ! check_canutils_installed; then
    apt update
    apt install -y can-utils
fi


# Step 1: Copy the start_usbtin.sh script to the appropriate location
echo "Copying start_usbtin.sh to $START_USBTIN_DEST..."
cp start_usbtin.sh $START_USBTIN_DEST
chmod +x $START_USBTIN_DEST

# Modify start_usbtin.sh to avoid premature exit and ensure it checks for slcan0
# This is a placeholder step. Ensure your script includes necessary checks or loops.

# Step 2: Add the udev rule
./install_udev_rules.sh


# Step 3: Create the systemd service file
echo "Creating systemd service for USBtin device..."
echo "[Unit]
Description=USBtin CAN Interface Service
After=network.target

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=$START_USBTIN_DEST
ExecStop=/sbin/ifconfig slcan0 down
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" | tee $SYSTEMD_SERVICE_FILE > /dev/null

# Reload the systemd daemon and enable the service
systemctl daemon-reload
systemctl enable usbtin.service

echo "Installation complete. The USBtin device will now be automatically started each time it's connected."
